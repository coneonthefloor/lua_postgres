-- load driver
local driver = require "luasql.postgres"
-- create environment object
env = assert (driver.postgres())
-- connect to data source
con = assert (env:connect("lua_messages"))
-- reset our table
cur = con:execute"SELECT * FROM messages;"
row = cur:fetch ({}, "a")
while row do
  print(string.format("MESSAGE: %s", row.content))
  -- reusing the table of results
  row = cur:fetch (row, "a")
end
-- close everything
cur:close() -- already closed because all the result set was consumed
con:close()
env:close()

